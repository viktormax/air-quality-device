#pragma once

#include <Adafruit_SSD1306.h>
#include <ESP8266WiFi.h>

bool connectWiFi(Adafruit_SSD1306& display);
