#pragma once

#include "credentials.h"

// #define DEBUG

#ifdef DEBUG
#define debug(format, args...) Serial.printf(format, ##args)
#else
#define debug(format, args...)
#endif

// 60 seconds
#define CYCLE_TIME_US (uint64_t)60e6

// #define SERVER_URL "http://192.168.0.181"

#define USE_HTTPS_CLIENT
#define SERVER_URL "https://d9s7lh7jt3ivn.cloudfront.net"
