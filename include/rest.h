#pragma once

#include <time.h>
#include <ArduinoJson.h>

#include "common.h"
#include "sensors.h"

bool sendData(data_t data, time_t timestamp, DynamicJsonDocument& commands);

bool sendExecutedCommand(String id);
