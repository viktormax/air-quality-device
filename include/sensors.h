#pragma once

struct data_t {
    float temperature;
    float humidity;
    float light;
    float co2;
};

void initSensors();

void setAutoCalibration(bool autoCalibration);

void calibrateZero();

data_t readData();