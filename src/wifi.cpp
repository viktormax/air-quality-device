#include "wifi.h"
#include "common.h"

bool connectWiFi(Adafruit_SSD1306& display) {
    unsigned long minRunningTimeMs = 100;

    auto start = millis();
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

    while (WiFi.status() != WL_CONNECTED) {
        delay(100);

        // If the cycle time has passed, we should not wait for WiFi.
        if (CYCLE_TIME_US < system_get_time()) {
            display.printf("WiFi time: NaN\n");
            return false;
        }
    }

    auto end = millis();
    auto elapsed = end - start;

    if (elapsed < minRunningTimeMs) {
        delay(minRunningTimeMs - elapsed);
    }

    display.printf("WiFi time: %lu\n", elapsed);
    return true;
}