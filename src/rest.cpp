#include "rest.h"

#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

#ifdef USE_HTTPS_CLIENT
#define BEGIN_HTTP(serverEndpoint)                                                    \
    HTTPClient http;                                                                  \
    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure); \
    client->setInsecure();                                                            \
    http.begin(*client, serverEndpoint);
#else
#define BEGIN_HTTP(serverEndpoint) \
    HTTPClient http;               \
    WiFiClient client;             \
    http.begin(client, serverEndpoint);
#endif

bool sendData(data_t data, time_t timestamp, DynamicJsonDocument& commands) {
    String serverEndpoint = SERVER_URL "/api/v1/devices/" DEVICE_UUID "/data";

    StaticJsonDocument<256> jsonDocument;
    jsonDocument["humidity"] = data.humidity;
    jsonDocument["temperature"] = data.temperature;
    jsonDocument["light"] = data.light;
    jsonDocument["co2"] = data.co2;
    jsonDocument["timestamp"] = timestamp;

    String jsonString;
    serializeJson(jsonDocument, jsonString);

    BEGIN_HTTP(serverEndpoint);
    http.addHeader("Content-Type", "application/json");

    debug("POST: %s \n%s\n", serverEndpoint.c_str(), jsonString.c_str());
    int httpCode = http.POST(jsonString);

    if (httpCode == HTTP_CODE_OK) {
        deserializeJson(commands, http.getString());
    }

    debug("POST result: %d - %s\n", httpCode, http.getString().c_str());

    http.end();

    return httpCode == HTTP_CODE_OK;
}

bool sendExecutedCommand(String id) {
    String serverEndpoint = SERVER_URL "/api/v1/devices/" DEVICE_UUID "/executed-commands/";

    BEGIN_HTTP(serverEndpoint + id);

    int httpCode = http.POST("");

    debug("POST result: %d - %s\n", httpCode, http.getString().c_str());

    http.end();
    return httpCode == HTTP_CODE_OK;
}
