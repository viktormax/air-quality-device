#include "sensors.h"

#include <BH1750.h>
#include <MHZ19.h>
#include <SHT2x.h>
#include <SoftwareSerial.h>

SoftwareSerial ss(14, 12);
MHZ19 mhz;
SHT2x sht;
BH1750 bh1750(0x23);

void initSensors() {
    Wire.begin();
    Serial.begin(115200);
    ss.begin(9600);
    sht.begin();
    mhz.begin(ss);  
    bh1750.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
}

void setAutoCalibration(bool autoCalibration) {
    mhz.autoCalibration(autoCalibration);
}

void calibrateZero() {
    mhz.calibrateZero();
}

data_t readData() {
    sht.read();

    data_t data;
    data.temperature = sht.getTemperature();
    data.humidity = sht.getHumidity();
    data.light = bh1750.readLightLevel();
    data.co2 = mhz.getCO2();

    return data;
}
