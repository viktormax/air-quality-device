#include <Arduino.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ezTime.h>

#include "common.h"
#include "rest.h"
#include "sensors.h"
#include "wifi.h"

#define SEND_DATA true

Adafruit_SSD1306 display(128, 64, &Wire);
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

uint64_t getSleepTimeUs();
void displayData(data_t data);
void executCommands(JsonArray&& commands);

void setup() {
    initSensors();

    timeClient.begin();
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

    delay(100);

    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0, 0);
}

void loop() {
    auto data = readData();
    displayData(data);

    if (connectWiFi(display)) {
        timeClient.update();

        if (SEND_DATA) {
            auto time = timeClient.getEpochTime();

            DynamicJsonDocument commands(256);
            if (sendData(data, time, commands) && commands.is<JsonArray>()) {
                executCommands(commands.as<JsonArray>());
            }
        }
    }

    display.printf("Cycle time: %lu\n", millis());
    display.printf("Time: %s\n", timeClient.getFormattedTime().c_str());
    display.display();

    ESP.deepSleep(getSleepTimeUs(), WAKE_NO_RFCAL);
}

void displayData(data_t data) {
    display.clearDisplay();
    display.setCursor(0, 0);
    display.printf("Temperature: %.2f\n", data.temperature);
    display.printf("Humidity: %.2f\n", data.humidity);
    display.printf("Light: %.2f\n", data.light);
    display.printf("CO2: %.0f\n", data.co2);
    display.display();
}

uint64_t getSleepTimeUs() {
    auto elapsed = system_get_time();

    return max(CYCLE_TIME_US - elapsed, (uint64_t)1e6);
}

void executCommands(JsonArray&& commands) {
    for (JsonVariant value : commands) {
        JsonObject obj = value.as<JsonObject>();
        String id = obj["id"].as<String>();
        String type = obj["type"].as<String>();

        debug("Command (%s) - %s\n", id.c_str(), type.c_str());

        if (type == "auto-calibration-on") {
            setAutoCalibration(true);
        } else if (type == "auto-calibration-off") {
            setAutoCalibration(false);
        } else if (type == "zero-calibration") {
            calibrateZero();
        } else {
            continue;
        }

        sendExecutedCommand(id);
    }
}
